<?php
/**
 * Plugin Name:     Rest API extensions
 * Plugin URI:      https://richardsweeney.com
 * Description:     Mini-plugin to extend the rest API
 * Author:          Richard Sweeney
 * Author URI:      https://richardsweeney.com
 * Version:         0.1.0
 *
 * @package         Rest_Api_Get_Post_By_Slug
 */

/**
 * Register my custom image size.
 */
add_action( 'after_setup_theme', function() {
	add_image_size( 'post_grid', 600, 300, true );
});

/**
 * Regiser the rest route.
 */
add_action( 'rest_api_init', function () {
	register_rest_route( 'rico-api/v1', '/(?P<slug>[a-zA-Z-]+)', [
		'methods'  => 'GET',
		'callback' => 'rico_api_get_post_by_slug',
	] );
} );

/**
 * Get and retrun a post by the slug.
 *
 * @param WP_REST_Request $request
 *
 * @return WP_REST_Response|WP_Error
 */
function rico_api_get_post_by_slug( WP_REST_Request $request ) {
	$slug = $request->get_param( 'slug' );
	if ( empty( $slug ) ) {
		return new WP_Error( 'rico_api_empty_slug', 'No slug provided' );
	}

	$post = get_page_by_path( $slug, OBJECT, 'post' );
	if ( ! $post ) {
		return new WP_Error( 'rico_api_post_not_found', 'Post not found', [
			'status' => 404,
		] );
	}

	$controller = new WP_REST_Posts_Controller( 'post' );
	return $controller->prepare_item_for_response( $post, $request );
}
