=== Rest Api Extensions ===
Contributors: theorboman
Tags: REST API
Requires at least: 4.9
Tested up to: 4.9
Stable tag: 0.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
